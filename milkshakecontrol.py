#!/usr/bin/env python3
# milkshakecontrol.py:
#                Wimborne Model Town
#      Milk Shake Factory Railway System Control Functions
#
#  **********************************************************************
# This program carries out the following functions:
#   With reference to this diagram:
#
#     Out -->                                                   <-- Back 
#
#  Tunnel End                                                  Factory End
#   ==  =============================================================  ==
#      ^                                                    ^
#      |                                                    |
#    Track                                                Sensor   Track
#    Break                                                         Break
#
#   1. On the press of a button on the guard-rail:
#
#   2. The Trackside Lights are switched on.
#
#   3. The train starts inside the tunnel and picks up speed in the Out direction 
#      taking approximately 2 seconds to reach full speed.
#      
#   4. The train then maintains a set speed down the track until it passes the Sensor,
#      when it reduces speed over approximately 2 seconds and then stops at the
#      Factory End.
#
#   5. After approximately 5 seconds, the train accelerates over approximately 2 seconds
#      and returns to the tunnel where it is stopped by the track break.
#
#   6. The Trackside Lights are switched off.
#
#
# Copyright (c) 2020 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>. 
#
#  ***********************************************************************

import RPi.GPIO as GPIO
import time

# Define Train Motor Control Pins
out_direction   = 17                    # Motor Board Pin IN1 Board Pin 11
back_direction   = 27                   # Motor Board Pin IN2 Board Pin 13
pwm = 18                                # PWM pin EnB Board Pin 12

# Define Train Position Detection Pins
sensor = 5                              # Sensor (Factory End) Board Pin 29 

# Define Trackside Lights Pin
trackside_lights = 22                   # Activate white LEDs via MOSFET

# Define Visitor Present Pin
button_pressed = 25                     # Visitor has pressed the button on the Guard Rail
                                        # Board Pin 21

# Define other variables (speed, wait times etc)
direction = "Out"                       # Initially Out 
max_speed_out = 50                      # Maximum Out Speed in percentage PWM (just over 10 V)
min_speed_out = 40                      # Minimum Out Speed in percentage PWM (just under 8 V)
max_speed_back = 50                     # Maximum Out Speed in percentage PWM (just over 10 V)
min_speed_back = 40                     # Maximum Out Speed in percentage PWM (just over 10 V)
stop_speed = 0                          # Speed to stop the train.
rate_of_change = 5                      # Size of PWM increments.
wait_time = 5                           # Define wait time at stations in seconds 
accel_time_out = 2                      # Define time to accelerate train out of the tunnel in seconds
accel_time_back = 2                     # Define time to accelerate train back into the tunnelin seconds
decel_time_out = 7                      # Define time to                   decelerate train at the factory in seconds
decel_time_back = 3                     # Define time to decelerate train in the tunnel in seconds
pause = 1                               # Pause after lights come on

#  Calculate how many times to continue checking if the train has passed the Sensor.
loops_pause_time = 0.25                # Time in seconds for the program to pause each time round the run time loops.

# Calculate number of loops in out leg.
sensor_test_time_out = 10                # Time in seconds required.
sensor_test_loops_out = (sensor_test_time_out / loops_pause_time)
print(sensor_test_loops_out)

# Calculate number of loops in back leg.
time_back = 3                            # Time in seconds required.
loops_back = (time_back / loops_pause_time)
#print(loops_pause_time)

# Overall control                       - may not be needed
num_loops_button_press = 1              # Set this to define how many times the train goes Out and Back after Button pressed
num_loops_rewarm = 4                    # Set this to define how many times the train goes Out and Back after a warmup cycle is triggered
cool_time = 10                          # Time in Minutes before a warm cycle commences.


# Initialise and setup starting conditions
GPIO.setmode(GPIO.BCM)                  # Numbers GPIOs by Broadcom Pin Numbers
GPIO.setup(out_direction, GPIO.OUT)     # Control pins for Motor Drive Board
GPIO.setup(back_direction, GPIO.OUT)    # ditto
GPIO.setup(pwm, GPIO.OUT)               # ditto

GPIO.setup(trackside_lights, GPIO.OUT)          # LED control pin

GPIO.setup(sensor, GPIO.IN, pull_up_down=GPIO.PUD_UP)           # Sensor pin

GPIO.setup(button_pressed, GPIO.IN, pull_up_down=GPIO.PUD_UP)     # Button pin

PWM = GPIO.PWM(pwm, 100)                # Create object PWM on pwm port at 1000 Hz
PWM.start(0)                            # Start motor on 0 percent duty cycle (off)

GPIO.output(out_direction, GPIO.LOW)    # reset both motor pins
GPIO.output(back_direction, GPIO.LOW)

GPIO.output(trackside_lights, GPIO.LOW) # Set lights to off

def train_start():
    global direction, accel_time_out, accel_time_back, max_speed_out, max_speed_back, min_speed_out, min_speed_back, rate_of_change, out_direction, back_direction
    if direction == "Out":
        # Accellerate train in the required direction
        # Calculate how long to delay each loop iteration to obtained the required acceleration time
        accel_loop_delay = accel_time_out/((max_speed_out - stop_speed)/rate_of_change)    
        speed = min_speed_out
        print("Train Start - Out")
        while speed <= max_speed_out:
            GPIO.output(out_direction, GPIO.HIGH)           # train down the track
            GPIO.output(back_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed + rate_of_change
            time.sleep(accel_loop_delay)
        PWM.ChangeDutyCycle(max_speed_out)                      # Allows for intermediate values of PWM.
    elif direction == "Back":
        # Decellerate train in the required direction
        accel_loop_delay = accel_time_back/((max_speed_back - stop_speed)/rate_of_change)    
        speed = min_speed_back
        print("Train Start - Back")
        while speed <= max_speed_back:
            GPIO.output(back_direction, GPIO.HIGH)          # train up the track
            GPIO.output(out_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed + rate_of_change
            time.sleep(accel_loop_delay)
        PWM.ChangeDutyCycle(max_speed_back)                      # Allows for intermediate values of PWM.


def train_stop():
    global direction, decel_time_out, max_speed_out, decel_time_back, max_speed_back, stop_speed, rate_of_change, out_direction, back_direction
    # Decellerate train to a stop
    if direction == "Out":
        # Calculate how long to delay each loop iteration to obtained the required deceleration time
        decel_loop_delay = decel_time_out/((max_speed_out - stop_speed)/rate_of_change)    
        speed = max_speed_out
        print("Train Stop - Out")
        while speed > stop_speed:
            GPIO.output(out_direction, GPIO.HIGH)          # train down the track
            GPIO.output(back_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed - rate_of_change
            time.sleep(decel_loop_delay)
        PWM.ChangeDutyCycle(stop_speed)                     # Allows for intermediate values of PWM.
    elif direction == "Back":
        # Calculate how long to delay each loop iteration to obtained the required deceleration time
        decel_loop_delay = decel_time_back/((max_speed_back - stop_speed)/rate_of_change)    
        speed = max_speed_back
        print("Train Stop - Back")
        while speed > stop_speed:
            GPIO.output(back_direction, GPIO.HIGH)          # train down the track
            GPIO.output(out_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed - rate_of_change
            time.sleep(decel_loop_delay)
        PWM.ChangeDutyCycle(stop_speed)                     # Allows for intermediate values of PWM.
   
def run_cycle(num_loops):
    global direction, trackside_lights, pause, sensor_test_loops_out, sensor, wait_time, loops_back

    loop_count = 0

    # Start at Store End and run "Out"
    while loop_count < num_loops:
        # Starting at the Store End Travel "Out" towards the Tunnel
        direction = "Out"

        GPIO.output(trackside_lights, GPIO.HIGH)        # Set Trackside Lights to On
        time.sleep(pause)                               # Pause a short time after the lights  come on

        print("Outward Leg.  Waiting for Sensor after speed reaches max.")

        train_start()                                   # Train will start and accellerate towards the Tunnel.

        i = sensor_test_loops_out                       # Set the loop counter to give the required time testing for the sensor

        while i >= 0:
            if GPIO.input(sensor):
                i -= 1
                time.sleep(0.25)
            else:
                print ("Sensor Activated")
                break

        train_stop()                                    # Train will decellerate and stop at the Factory area.
       
        time.sleep(wait_time)                           #Train will wait at the Factory.
        direction = "Back"
        print("Back Leg.  Run until track break or timeout.")

        train_start()                                   # Train will start, accellerate and stop 

        i = loops_back                                  # Set the loop counter to give enough time for the train to return

        while i >= 0:
            i -= 1
            time.sleep(0.25)
        train_stop()

        GPIO.output(trackside_lights, GPIO.LOW)        # Set Trackside Lights to Off

        loop_count += 1

def destroy():
    GPIO.output(trackside_lights, GPIO.LOW)                     # Set Trackside Lights to Off
    GPIO.cleanup()                                              # Release resource

if __name__ == '__main__':                                      # Program start from here
    try:
        print("Program Start")
        print("Press the Guardrail Button")
        while True:
            # Wait for button to be pressed
            if not GPIO.input(button_pressed):                  # Line pulled high if button not pressed; go round again.
                print ("Button Pressed")
                run_cycle(num_loops_button_press)               # Button press detected, do normal cycle
                print("Press the Guardrail Button")
            else:
                time.sleep(0.25)                                # Keep waiting until Button pressed
                print("Waiting")

    except KeyboardInterrupt:                                   # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
